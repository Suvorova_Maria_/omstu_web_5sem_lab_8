import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { AppRoutes } from '../../common/constants';
import { Post as PostType } from '../../common/types';
import { CommentsList } from './components';
import './index.css';

export function Post() {
  const [post, setPost] = useState<PostType | null>(null);

  /**
   * Получаем ID нужного поста из текущего url-а
   */
  const { id } = useParams();

  const fetchPost = async () => {
    if (!id) {
      return;
    }

    /**
     * const post = API GET POST BY ID
     * const author = API GET USER BY ID
     * const comments = API GET COMMENTS BY POST_ID
     */

    /**
     * Не забудьте в post в поле user записать полученного author,
     *  а в поле comments полученные comments
     *
     * Example: post.author = user;
     */
  };

  useEffect(() => {
    /**
     * слушаем изменение ID из текущего url-а
     * на каждое изменение загружаем пост с таким ID
     * чтобы при изменении ID из url-а подгружался новый пост
     */
  }, [id]);

  if (!post) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Post№ {post.id}</h1>
      <a href={AppRoutes.PROFILE(post.userId)}>{post.user?.name}</a>
      <h2>{post.title}</h2>
      <p>{post.body}</p>
      <CommentsList comments={post.comments} />
    </div>
  );
}
